# static-starter

简单的基于gulp的静态多页面工程，可以支持less编写样式，hbs编写html页面。自动启动服务器，自动刷新和监听文件变化。


## 快速开始

```
git clone git@git.oschina.net:jackchoon/static-starter.git
cd static-starter
npm install
```

### 常用指令

##### 开发
```
npm run start 或者 gulp
```

##### 编译到静态目录 ./build
```
npm run build 
```

##### 部署到远程服务器
```
# 部署到测试环境
npm run staging
 
# 部署到正式环境
npm run deploy
```